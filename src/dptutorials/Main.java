package dptutorials;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * @author Matti Martikainen
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("SearchView.fxml"));
        primaryStage.setTitle("Design Pattern Helper");
        primaryStage.setScene(new Scene(root, 600, 200));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
