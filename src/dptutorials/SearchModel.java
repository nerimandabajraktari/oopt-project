package dptutorials;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Contains business logic for the search window.
 * @author Matti Martikainen
 */
class SearchModel {
    // Contains data for all available tutorials.
    private final ArrayList<TutorialData> tutorials;

    // Contains tags the user has searched for.
    private final ArrayList<String> tags = new ArrayList<>();

    // A list of tutorials that match the user's search. Observed by the ListView.
    private final ObservableList<TutorialData> searchResults = FXCollections.observableArrayList();

    // Contains the tags together as a single string, separated with commas. Observed by the tags label.
    private final StringProperty tagStringProperty = new SimpleStringProperty();

    public SearchModel() {
        tutorials = parseXMLData();
        searchResults.addAll(tutorials);
    }

    // Parses the list of tutorials from content/tutorials.xml.
    private ArrayList<TutorialData> parseXMLData() {
        ArrayList<TutorialData> data = new ArrayList<>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            Document doc = documentBuilder.parse(getClass().getResource("content/tutorials.xml").toExternalForm());
            doc.getDocumentElement().normalize();

            NodeList nodes = doc.getElementsByTagName("tutorial");

            for (int i = 0; i < nodes.getLength(); i++) {
                Element e = (Element) nodes.item(i);

                String name = e.getElementsByTagName("name").item(0).getTextContent();
                String shortDescription = e.getElementsByTagName("shortDescription").item(0).getTextContent();
                String htmlPath = "content/" + e.getElementsByTagName("fileName").item(0).getTextContent();
                ArrayList<String> tags = new ArrayList<>();

                NodeList tagNodes = e.getElementsByTagName("tag");
                for (int j = 0; j < tagNodes.getLength(); j++) {
                    String tag = tagNodes.item(j).getTextContent();
                    tags.add(tag);
                }

                TutorialData d = new TutorialData(name, tags, shortDescription, htmlPath);
                data.add(d);
            }

        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }

        return data;
    }

    // Adds a tag.
    void addTag(String tag) {
        tags.add(tag);
        updateTagStringProperty();
        search();
    }

    // Clears all tags.
    void clearTags() {
        tags.clear();
        updateTagStringProperty();
        search();
    }

    // Updates the tags StringProperty bound to the view.
    private void updateTagStringProperty() {
        tagStringProperty.setValue(TutorialData.formatTagsAsString(tags));
    }

    // Filters the tutorials list with the tags the user has entered.
    private void search() {
        ArrayList<TutorialData> filtered = new ArrayList<>(tutorials);

        if (!tags.isEmpty()) {
            for (String tag : tags) {
                for (TutorialData d : tutorials) {
                    if (!d.hasTag(tag)) {
                        filtered.remove(d);
                    }
                }
            }
        }

        searchResults.clear();
        searchResults.addAll(filtered);
    }

    ObservableList<TutorialData> getSearchResults() {
        return searchResults;
    }

    StringProperty getTagStringProperty() {
        return tagStringProperty;
    }
}
