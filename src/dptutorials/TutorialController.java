package dptutorials;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * Controller for the tutorial popup window.
 * @author Matti Martikainen
 */
public class TutorialController {
    @FXML
    private Button closeBtn;
    @FXML
    private WebView webView;

    // Loads a html file to the WebView. Remember to call before showing the window!
    void loadHtml(String filename) {
        webView.getEngine().load(getClass().getResource(filename).toExternalForm());
    }

    @FXML
    private void closeWindow() {
        Stage stage = (Stage) closeBtn.getScene().getWindow();
        stage.close();
    }
}
