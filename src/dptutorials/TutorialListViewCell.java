package dptutorials;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;
import javafx.scene.text.*;

import java.io.IOException;

/**
 * A custom ListView cell with formatted text.
 * Tutorial source: https://www.turais.de/how-to-custom-listview-cell-in-javafx/
 * @author Matti Martikainen
 */
class TutorialListViewCell extends ListCell<TutorialData> {
    @FXML
    private GridPane gridPane;
    @FXML
    private TextFlow text1;
    @FXML
    private TextFlow text2;

    private FXMLLoader loader;

    @Override
    protected void updateItem(TutorialData tutorial, boolean empty) {
        super.updateItem(tutorial, empty);

        if (empty || tutorial == null) {

            setText(null);
            setGraphic(null);

        } else {
            if (loader == null) {
                loader = new FXMLLoader(getClass().getResource("TutorialListViewCell.fxml"));
                loader.setController(this);

                try {
                    loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            Text upper1 = new Text(tutorial.getPatternName() + " ");
            Text upper2 = new Text(tutorial.getShortDescription());
            upper1.setFont(Font.font("Segoe UI", FontWeight.BOLD, 12));
            text1.getChildren().clear();
            text1.getChildren().addAll(upper1, upper2);

            Text lower1 = new Text("Tags: ");
            Text lower2 = new Text(TutorialData.formatTagsAsString(tutorial.getTags()));
            lower2.setFont(Font.font("Segoe UI", FontPosture.ITALIC, 12));
            text2.getChildren().clear();
            text2.getChildren().addAll(lower1, lower2);

            setText(null);
            setGraphic(gridPane);
        }

    }
}
