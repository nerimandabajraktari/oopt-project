package dptutorials;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

/**
 * Controller for SearchView (the main window).
 * @author Matti Martikainen
 */
public class SearchController {
    @FXML
    private Label tagsLbl;
    @FXML
    private TextField searchBar;
    @FXML
    private ListView<TutorialData> tutorialListView = new ListView<>();

    private final SearchModel searchModel = new SearchModel();

    @FXML
    private void initialize() {
        // Set up view components to observe changes in the model data.
        tutorialListView.setItems(searchModel.getSearchResults());
        tagsLbl.textProperty().bind(searchModel.getTagStringProperty());

        // don't hide prompt text until user starts typing
        searchBar.setStyle("-fx-prompt-text-fill: derive(-fx-control-inner-background, -30%);");

        // use custom list cells
        tutorialListView.setCellFactory(listView -> new TutorialListViewCell());
    }

    // Adds a tag from the search bar to the search model.
    @FXML
    private void addTag() {
        if (!Objects.equals(searchBar.getText(), "")) {
            searchModel.addTag(searchBar.getText());
            searchBar.clear();
        }
    }

    // Clears all tags.
    @FXML
    private void clearTags() {
        searchModel.clearTags();
    }

    // Opens a tutorial corresponding to the currently selected list item in a new window.
    @FXML
    private void openTutorial() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("TutorialView.fxml"));
        Parent root = loader.load();

        TutorialController controller = loader.getController();
        controller.loadHtml(tutorialListView.getSelectionModel().getSelectedItem().getHtmlPath());

        Stage stage = new Stage();
        stage.setTitle("Tutorial");
        stage.setScene(new Scene(root, 600, 800));
        stage.show();
    }
}
